<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'HomeController@index')->name("main");
Route::get('/home', 'HomeController@index')->name("mainHome");

/**
 * Auth
 */
Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
Route::post('/login', 'AdminAuth\LoginController@login')->name("postLogin");
Route::get('/logout', 'AdminAuth\LoginController@logout')->name("logout");

Route::get('/users', 'UserController@index')->name("user.index");
Route::post('/users/create', 'UserController@create')->name("user.create");
Route::get('/users/{id}/edit', 'UserController@edit')->name("user.edit");
Route::put('/users/{id}/update', 'UserController@update')->name("user.update");
Route::delete('/users/{id}/delete', 'UserController@destroy')->name("user.delete");
Route::post('/users/datatables', 'UserController@tableUsers')->name('tableUsers');

/**
 * Route Training
 */
Route::get('/training', 'TrainingController@index')->name('training.index');
Route::post('/training/create', 'TrainingController@create')->name('training.create');
Route::post('/training/store', 'TrainingController@store')->name('training.store');
Route::post('/training/datatables', 'TrainingController@tabletraining')->name('tabletraining');
Route::get('/training/{id}/edit', 'TrainingController@edit')->name('training.edit');
Route::put('/training/{id}/update', 'TrainingController@update')->name('training.update');
Route::delete('/training/{id}/delete', 'TrainingController@deleteTraining')->name('deleteTraining');
Route::post('/training/importexcel', 'TrainingController@importExcel')->name('training.importExcel');


/**
 * Route Pengajuan1
 */
Route::get('/pengajuan', 'Pengajuan1Controller@index')->name('pengajuan.index');
Route::get('/pengajuan/create', 'Pengajuan1Controller@create')->name('pengajuan.create');
Route::post('/pengajuan/store', 'Pengajuan1Controller@store')->name('pengajuan.store');
Route::post('/pengajuan/datatables', 'Pengajuan1Controller@tablepengajuan')->name('tablepengajuan');
Route::post('/pengajuan/importexcel', 'Pengajuan1Controller@importExcel')->name('pengajuan.importExcel');

//Route::get('/training/{id}/edit', 'TrainingController@edit')->name('training.edit');
//Route::put('/training/{id}/update', 'TrainingController@update')->name('training.update');
//Route::delete('/training/{id}/delete', 'TrainingController@deletetraining')->name('deleteFormulir');



/**
 * Route Karyawan
 */
Route::get('/karyawan', 'karyawanController@index')->name('karyawan.index');
Route::post('/karyawan/create', 'karyawanController@create')->name('karyawan.create');
Route::post('/karyawan/store', 'karyawanController@store')->name("karyawan.store");
Route::post('/karyawan/datatables', 'karyawanController@tablekaryawan')->name('tablekaryawan');
Route::get('/karyawan/{id}/edit', 'karyawanController@edit')->name('karyawan.edit');
Route::put('/karyawan/{id}/update', 'karyawanController@update')->name('karyawan.update');
Route::delete('/karyawan/{id}/delete', 'karyawanController@deleteKaryawan')->name('deleteKaryawan');
/**
 * Route Siswa
 */
Route::get('/siswa','SiswaController@index')->name('siswa.index');
Route::post('/siswa/create', 'SiswaController@create')->name('siswa.create');
Route::post('/siswa/store', 'SiswaController@store')->name("siswa.store");
Route::post('/siswa/datatables', 'SiswaController@tablesiswa')->name('tablesiswa');
Route::get('/siswa/{id}/edit', 'SiswaController@edit')->name('siswa.edit');
Route::put('/siswa/{id}/update', 'SiswaController@update')->name('siswa.update');
Route::delete('/siswa/{id}/delete', 'SiswaController@destroy')->name('siswa.delete');
Route::get('/siswa/{id}/profile', 'SiswaController@profile')->name('profil.siswa');
Route::post('/siswa/datasiswa', 'SiswaController@datasiswa')->name('datasiswa');
/**
 * Route Jurusan
 */
Route::get('/jurusan','JurusanController@index')->name('jurusan.index');
Route::post('/jurusan/datatables', 'JurusanController@tablejurusan')->name('tablejurusan');
Route::post('/jurusan/create', 'JurusanController@create')->name('jurusan.create');
Route::get('/jurusan/{id}/edit', 'JurusanController@edit')->name('jurusan.edit');
Route::put('/jurusan/{id}/update', 'JurusanController@update')->name('jurusan.update');
Route::delete('/jurusan/{id}/delete', 'JurusanController@destroy')->name('jurusan.delete');
/**
 * Route Kelas
 */
Route::get('/kelas','KelasController@index')->name('kelas.index');
Route::post('/kelas/datatables', 'KelasController@tablejurusan')->name('tablekelas');
Route::post('/kelas/create', 'KelasController@create')->name('kelas.create');
Route::get('/kelas/{id}/edit', 'KelasController@edit')->name('kelas.edit');
Route::put('/kelas/{id}/update', 'KelasController@update')->name('kelas.update');
Route::delete('/kelas/{id}/delete', 'KelasController@destroy')->name('kelas.delete');
/**
 * Route Guru
 */
Route::get('/guru','GuruController@index')->name('guru.index');
Route::post('/guru/datatables', 'GuruController@tableguru')->name('tableguru');
Route::post('/guru/create', 'GuruController@create')->name('guru.create');
Route::post('/guru/store', 'GuruController@store')->name("guru.store");
Route::get('/guru/{id}/edit', 'GuruController@edit')->name('guru.edit');
Route::put('/guru/{id}/update', 'GuruController@update')->name('guru.update');
Route::delete('/guru/{id}/delete', 'GuruController@destroy')->name('guru.delete');
/**
 * Route Pelajaran
 */
Route::get('/pelajaran','PelajaranController@index')->name('pelajaran.index');
Route::post('/pelajaran/datatables', 'PelajaranController@tablejurusan')->name('tablepelajaran');
Route::post('/pelajaran/create', 'PelajaranController@create')->name('pelajaran.create');
Route::get('/pelajaran/{id}/edit', 'PelajaranController@edit')->name('pelajaran.edit');
Route::put('/pelajaran/{id}/update', 'PelajaranController@update')->name('pelajaran.update');
Route::delete('/pelajaran/{id}/delete', 'PelajaranController@destroy')->name('pelajaran.delete');
/**
 * Route Jadwal Pelajaran
 */
Route::get('/jadwal','JadwalController@index')->name('jadwal.index');
Route::post('/jadwal/datatables', 'JadwalController@tablejadwal')->name('tablejadwal');
Route::post('/jadwal/create', 'JadwalController@create')->name('jadwal.create');
Route::get('/jadwal/{id}/edit', 'JadwalController@edit')->name('jadwal.edit');
Route::put('/jadwal/{id}/update', 'JadwalController@update')->name('jadwal.update');
Route::delete('/jadwal/{id}/delete', 'JadwalController@destroy')->name('jadwal.delete');
Route::post('/jabar','JadwalController@jabar')->name('jabar');
/** 
 * Route Item
 */
Route::get('importExport', 'MaatwebsiteDemoController@importExport');
Route::get('downloadExcel/{type}', 'MaatwebsiteDemoController@downloadExcel');
Route::post('importExcel', 'MaatwebsiteDemoController@importExcel');