<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JadwalModel extends Model
{
    protected $table = 'jadwal';
    protected $fillable = [
        'id',
        'tanggal',
        'pelajaran',
        'guru',
        'jam1',
        'jam2'
    ];
    public function guru(){
        return $this->belongsTo('App\Models\GuruModel', 'guru');
    }
    public function pelajaran()
    {
        return $this->belongsTo('App\Models\PelajaranModel','pelajaran');
    }
}
