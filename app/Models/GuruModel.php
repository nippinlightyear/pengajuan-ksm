<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuruModel extends Model
{
    protected $table = 'guru';
    protected $fillable = [
        'id',
        'nip',
        'nama',
        'nohp',
        'pelajaran'
    ]; 
    public function pelajaran(){
        return $this->belongsTo('App\Models\PelajaranModel', 'pelajaran');
    }
}
