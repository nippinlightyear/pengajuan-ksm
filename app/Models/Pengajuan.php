<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
     protected $table = 'pengajuan';
     protected $fillable = [ 
        'id',
        'nama',
        'nik',
        'alamat',
        'nama_perusahan',
        'gaji',
        'limit',
        'tenor',
        'kolektibilitas',
        'tunjangan_kerja',
        'insentif',
        'bunga',
        'dsr',
        'angsuran'
    ]; 


}
