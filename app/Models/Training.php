<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
     protected $table = 'training';
     protected $fillable = ['id',
        'no_rek',
        'nama_nasabah',
        'alamat',
        'no_tlp',
        'nama_perusahan',
        'jenis_perusahaan',
        'limit',
        'kolektibilitas',
        'tenor'
        
    ]; 


}
