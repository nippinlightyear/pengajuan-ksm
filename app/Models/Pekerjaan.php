<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{

    /**
     * @var string
     */
    protected $table = 'pekerjaan';
     protected $fillable = [ 
        // A.Data Pekerjaan Pemohon
        'id',
        'nama_perusahaan_pemohon',
        'alamat_kantor_pemohon'
        
    ]; 
}