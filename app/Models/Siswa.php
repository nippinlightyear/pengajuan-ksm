<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = [
        'id',
        'nama',
        'kelas',
        'jurusan',
        'alamat',
        'nama_ortu',
        'avatar'
    ]; 
    public function jurusan(){
        return $this->belongsTo('App\Models\Jurusan', 'jurusan');
    }
    public function kelas()
    {
        return $this->belongsTo('App\Models\KelasModel','kelas');
    }
}
