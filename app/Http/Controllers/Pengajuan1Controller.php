<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;
use App\Models\Pengajuan;
use App\Models\Training;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Excel;
use DB;

class Pengajuan1Controller extends Controller
{
     public function index()
    {
        // 1 Admin App , 2 Admin Sekolah
    //     if (Auth::user()->role_id !== 2){
    //         return abort(401); 
    // }
       $idKolektibiitas = Training::select('kolektibilitas')->distinct()->get();
       $nama_perusahaan = Training::select('nama_perusahaan')->distinct()->get();


        $kolektibilitas1 = Training::where('kolektibilitas', '=', '1')->count();
        $kolektibilitas2A = Training::where('kolektibilitas', '=', '2A')->count();
        $kolektibilitas2B = Training::where('kolektibilitas', '=', '2B')->count();
        $kolektibilitas2C = Training::where('kolektibilitas', '=', '2C')->count();
        $kolektibilitas3 = Training::where('kolektibilitas', '=', '3')->count();
        $kolektibilitas4 = Training::where('kolektibilitas', '=', '4')->count();
        $kolektibilitas5 = Training::where('kolektibilitas', '=', '5')->count();
        

        $dataKol = [
            '1' => $kolektibilitas1,
            '2A' => $kolektibilitas2A,
            '2B' => $kolektibilitas2B,
            '2C' => $kolektibilitas2C,
            '3' => $kolektibilitas3,
            '4' => $kolektibilitas4,
            '5' => $kolektibilitas5,

        ];
        //dd($dataKol);
        

        // foreach ($kolektibilitas as $idKol) {
        //     $allKol[] = DB::table('training')
        //      ->select(DB::raw('count(*) as data_count, kolektibilitas'))
        //      ->where('kolektibilitas', '=', $idKol->kolektibilitas)
        //      ->groupBy('id')
        //      ->get();
        // }
        
        //     dd($allKol);
    
        // salma
        // dd($idKolektibiitas->toArray());
//        foreach ($nama_perusahaan as $naper) {
            foreach ($idKolektibiitas as $idKol) {
            
                $users[] = DB::table('training')
                 ->select(DB::raw('count(*) as data_count, kolektibilitas, data_limit'))
                 ->where('kolektibilitas', '=', $idKol->kolektibilitas)
                 ->groupBy('data_limit')
                 ->get();
            }
        //}
        //dd($users);


        $pengajuan = Pengajuan::all();        
        return view('pengajuan1.index');
    }

     public function tablepengajuan()
    {
        $pengajuan = Pengajuan::all();


        return Datatables::of($pengajuan)->make(true);
    }

    public function create(Request $request)
    {       
        $idKolektibiitas = Training::select('kolektibilitas')->distinct()->get();


        $kolektibilitas1 = Training::where('kolektibilitas', '=', '1')->count();
        $kolektibilitas2A = Training::where('kolektibilitas', '=', '2A')->count();
        $kolektibilitas2B = Training::where('kolektibilitas', '=', '2B')->count();
        $kolektibilitas2C = Training::where('kolektibilitas', '=', '2C')->count();
        $kolektibilitas3 = Training::where('kolektibilitas', '=', '3')->count();
        $kolektibilitas4 = Training::where('kolektibilitas', '=', '4')->count();
        $kolektibilitas5 = Training::where('kolektibilitas', '=', '5')->count();
        
        foreach ($idKolektibiitas as $idKol) {
            $nama_perusahaans[] = DB::table('training')
                 ->select(DB::raw('count(*) as data_count, kolektibilitas, nama_perusahaan'))
                 ->where('kolektibilitas', '=', $idKol->kolektibilitas)
                 ->groupBy('nama_perusahaan')
                 ->get();

            $data_limits[] = DB::table('training')
                 ->select(DB::raw('count(*) as data_count, kolektibilitas, data_limit'))
                 ->where('kolektibilitas', '=', $idKol->kolektibilitas)
                 ->groupBy('data_limit')
                 ->get();

            $tenors[] = DB::table('training')
                 ->select(DB::raw('count(*) as data_count, kolektibilitas, tenor'))
                 ->where('kolektibilitas', '=', $idKol->kolektibilitas)
                 ->groupBy('tenor')
                 ->get();
        }

        $dataKol = [
            '01' => $kolektibilitas1,
            '2A' => $kolektibilitas2A,
            '2B' => $kolektibilitas2B,
            '2C' => $kolektibilitas2C,
            '03' => $kolektibilitas3,
            '04' => $kolektibilitas4,
            '05' => $kolektibilitas5,
            '01np' =>  $nama_perusahaans[0]->count(),
            '2Anp' =>  $nama_perusahaans[1]->count(),
            '2Bnp' =>  $nama_perusahaans[2]->count(),
            '2Cnp' =>  $nama_perusahaans[3]->count(),
            '03np' =>  $nama_perusahaans[4]->count(),
            '04np' =>  $nama_perusahaans[5]->count(),
            '05np' =>  $nama_perusahaans[6]->count(),
            '01dl' =>  $data_limits[0]->count(),
            '2Adl' =>  $data_limits[1]->count(),
            '2Bdl' =>  $data_limits[2]->count(),
            '2Cdl' =>  $data_limits[3]->count(),
            '03dl' =>  $data_limits[4]->count(),
            '04dl' =>  $data_limits[5]->count(),
            '05dl' =>  $data_limits[6]->count(),
            '01t' =>  $tenors[0]->count(),
            '2At' =>  $tenors[1]->count(),
            '2Bt' =>  $tenors[2]->count(),
            '2Ct' =>  $tenors[3]->count(),
            '03t' =>  $tenors[4]->count(),
            '04t' =>  $tenors[5]->count(),
            '05t' =>  $tenors[6]->count(),
        ];
        //dd($dataKol);
        

        // foreach ($kolektibilitas as $idKol) {
        //     $allKol[] = DB::table('training')
        //      ->select(DB::raw('count(*) as data_count, kolektibilitas'))
        //      ->where('kolektibilitas', '=', $idKol->kolektibilitas)
        //      ->groupBy('id')
        //      ->get();
        // }
        
        //     dd($allKol);
    
        // salma
        // dd($idKolektibiitas->toArray());
        foreach ($idKolektibiitas as $idKol) {
        
            $users[] = DB::table('training')
             ->select(DB::raw('count(*) as data_count, kolektibilitas, nama_perusahaan'))
             ->where('kolektibilitas', '=', $idKol->kolektibilitas)
             ->groupBy('id')
             ->get();
        }

        // $pengajuan = Pengajuan::all();
        return view('pengajuan1.create', compact('dataKol'));
    }

      public function store(Request $request)
    {
        //dd($karyawan);
        $validator = Validator::make($request->all(),
            [
            'nama' => 'required|max:255',
            'nik' => 'required|max:255',
            'alamat' => 'required|max:255',
            'nama_perusahaan' => 'required|max:255',
            'gaji' => 'required|max:255',
            'limit' => 'required|max:255',
            'tenor' => 'required|max:255',
            'kolektibilitas' => 'required|max:255',
            'insentif' => 'required|max:255',
            'bunga' => 'required|max:255',
            // 'dsr' => 'required|max:255',
            // 'angsuran' => 'required|max:255',
            // 'status' => 'required|max:255',
            // 'nk1' => 'required|max:255',
            // 'nk2a' => 'required|max:255',
            // 'nk2b' => 'required|max:255',
            // 'nk2c' => 'required|max:255',
            // 'nk3' => 'required|max:255',
            // 'nk4' => 'required|max:255',
            // 'nk5' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            DB::beginTransaction();
            try {
                $pengajuan = new Pengajuan;
                $pengajuan->nama = $request->nama;
                $pengajuan->nik = $request->nik;
                $pengajuan->alamat = $request->alamat;
                $pengajuan->nama_perusahaan = $request->nama_perusahaan;
                $pengajuan->gaji = $request->gaji;
                $pengajuan->tunjangan_kerja = $request->tunjangan_kerja;
                $pengajuan->insentif = $request->insentif;
                $pengajuan->limit = $request->limit;
                $pengajuan->tenor = $request->tenor;
                $pengajuan->bunga = $request->bunga;
                $pengajuan->kolektibilitas = $request->kolektibilitas;
                // $pengajuan->dsr = $request->dsr;
                // $pengajuan->angsuran = $request->angsuran;
                // $pengajuan->status = $request->status;
                // $pengajuan->nk1 = $request->nk1;
                // $pengajuan->nk2a = $request->nk2a;
                // $pengajuan->nk2b = $request->nk2b;
                // $pengajuan->nk2c = $request->nk2c;
                // $pengajuan->nk3 = $request->nk3;
                // $pengajuan->nk4 = $request->nk4;
                // $pengajuan->nk5 = $request->nk5;
                $pengajuan->save();

               
           
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollback();
                return showResponseError(500, ['error' => $ex->getMessage()] );
            }

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }

    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file')){
            // dd("wew");
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                // dd($data);
                $data= json_decode($data,true); 
                foreach ($data as $key => $value) {
                    $insert[] = [
                        'nama' => $value['nama'],
                        'nik' => $value['nik'],
                        'alamat' => $value['alamat'],
                        'nama_perusahaan' => $value['nama_perusahaan'],
                        'gaji' => $value['gaji'], 
                        'tunjangan_kerja' => $value['tunjangan_kerja'],
                        'insentif' => $value['insentif'],
                        'limit' => $value['limit'],
                        'tenor' => $value['tenor'], 
                        'bunga' => $value['bunga'],
                        'kolektibilitas' => $value['kolektibilitas'],
                        'dsr' => $value['dsr'],
                        'angsuran' => $value['angsuran'],
                        'status' => $value['status'], 
                         
                        
                    ];
                }
                // dd($insert);
                if(!empty($insert)){
                    DB::table('pengajuan')->insert($insert);
                    Session::flash('message', 'Import data berhasil.'); 
                    Session::flash('alert-class', 'alert-success'); 
                    return redirect()->back();
                }
            }
        }
        return back();
    }
   
}
