<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Session;
use App\Models\Training;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Excel;
use DB;

class TrainingController extends Controller
{
     public function index()
    {
        // 1 Admin App , 2 Admin Sekolah
    //     if (Auth::user()->role_id !== 2){
    //         return abort(401); 
    // }
        
        return view('training.index1');
    }

    public function tabletraining()
    {
        $training = Training::all();

        return Datatables::of($training)->make(true);
    }

   public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'no_rek' => 'required|max:255',
            'nama_nasabah' => 'required|max:255',
            'alamat' => 'required|max:255',
            'no_tlp' => 'required|max:255',
            'nama_perusahaan' => 'required|max:255',
            'data_limit' => 'required|max:255',
            'kolektibilitas' => 'required|max:255',
            'tenor' => 'required|max:255',
            'penghasilan' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $training = new Training;
            $training->no_rek = $request->no_rek;
            $training->nama_nasabah = $request->nama_nasabah;
            $training->alamat = $request->alamat;
            $training->no_tlp = $request->no_tlp;
            $training->nama_perusahaan = $request->nama_perusahaan;
            $training->data_limit = $request->data_limit;
            $training->kolektibilitas = $request->kolektibilitas;
            $training->tenor = $request->tenor;
            $training->penghasilan = $request->penghasilan;
            $training->save();

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }

    public function edit($id)
   {
    $training = training::findOrFail($id);
    return showResponseSuccess($training);
   }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'no_rek' => 'required|max:255',
            'nama_nasabah' => 'required|max:255',
            'alamat' => 'required|max:255',
            'no_tlp' => 'required|max:255',
            'nama_perusahaan' => 'required|max:255',
            'data_limit' => 'required|max:255',
            'kolektibilitas' => 'required|max:255',
            'tenor' => 'required|max:255',
            'penghasilan' => 'required|max:255',

           
        ]);

        if ($validator->passes()) {

            $training = Training::findOrFail($id);


            $training->no_rek = $request->no_rek;
            $training->nama_nasabah = $request->nama_nasabah;
            $training->alamat = $request->alamat;
            $training->no_tlp = $request->no_tlp;
            $training->nama_perusahaan = $request->nama_perusahaan;
            $training->data_limit = $request->data_limit;
            $training->kolektibilitas = $request->kolektibilitas;
            $training->tenor = $request->tenor;
            $training->penghasilan = $request->penghasilan;
            
            $training->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());

    }

   public function deletetraining(Request $req)
    {
        if (!$req->has(['id'])) {
            return showResponseError(400, 'Parameter ID tidak tersedia');
        }

        $training = $req->only(['id']);

        $result = Training::where($training)->delete();

        if ($result) {
            return showResponseSuccess($result);
        } else {
            return showResponseError($result);
        }
    }

     public function store (Request $request)
    {
        dd($training);
        $validator = Validator::make($request->all(),
            [
            // Data no_tlp
            'no_rek' => 'required|max:255',
            'nama_nasabah' => 'required|max:255',
            'alamat' => 'required|max:255',
            'no_tlp' => 'required|max:255',
            'nama_perusahaan' => 'required|max:255',
            'data_limit' => 'required|max:255',
            'kolektibilitas' => 'required|max:255',
            'tenor' => 'required|max:255',
            'penghasilan' => 'required|max:255',

           
        ]);

        if ($validator->passes()) {

            DB::beginTransaction();
            try {
                

                $training = new training;
                $training->no_rek = $request->no_rek;
                $training->nama_nasabah = $request->nama_nasabah;
                $training->alamat = $request->alamat;
                $training->no_tlp = $request->no_tlp;
                $training->nama_perusahaan = $request->nama_perusahaan;
                $training->data_limit = $request->data_limit;
                $training->kolektibilitas = $request->kolektibilitas;
                $training->tenor = $request->tenor;
                $training->penghasilan = $request->penghasilan;
                //$siswa->created_by = Auth::id();
                //$siswa->updated_by = Auth::id();
               
                $training->save();
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollback();
                return showResponseError(500, ['error' => $ex->getMessage()] );
            }

            return showResponseSuccess(200, 'Data berhasil di perbarui');
        }
        return showResponseError(404, $validator->errors());
    }
    
    public function importExcel(Request $request)
    {
        if($request->hasFile('import_file')){
            // dd("wew");
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                // dd($data);
                $data= json_decode($data,true); 
                foreach ($data as $key => $value) {
                    $insert[] = [
                        'no_rek' => $value['no_rek'],
                        'nama_nasabah' => $value['nama_nasabah'],
                        'alamat' => $value['alamat'],
                        'no_tlp' => $value['no_tlp'],
                        'nama_perusahaan' => $value['nama_perusahaan'],
                        'data_limit' => $value['data_limit'],
                        'kolektibilitas' => $value['kolektibilitas'],
                        'tenor' => $value['tenor'], 
                        'penghasilan' => $value['penghasilan'], 
                         
                        
                    ];
                }
                // dd($insert);
                if(!empty($insert)){
                    DB::table('training')->insert($insert);
                    Session::flash('message', 'Import data berhasil.'); 
                    Session::flash('alert-class', 'alert-success'); 
                    return redirect()->back();
                }
            }
        }
        return back();
    }
}
