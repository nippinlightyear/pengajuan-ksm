<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GuruModel;
use App\Models\PelajaranModel;
use App\Models\JadwalModel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use DB;
use Image;
use Illuminate\Support\Facades\Auth;

class JadwalController extends Controller
{
    public function index()
    {
        $guru = GuruModel::all();
        $pelajaran = PelajaranModel::all();
        return view('jadwalpelajaran.index',compact('guru','pelajaran'));
    }
    public function tablejadwal()
    {
       
        $jadwal = JadwalModel::with('guru','pelajaran');
        // dd($siswa);
        return Datatables::of($jadwal)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'pelajaran' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
            // 'avatar'=> 'required',
        ]);
        if ($validator->passes()) {
            $jadwal = new JadwalModel;
            $jadwal->tanggal = $request->tanggal;
            $jadwal->pelajaran = $request->pelajaran;
            $jadwal->guru = $request->guru;
            $jadwal->jam1 = $request->jam1;
            $jadwal->jam2 = $request->jam2;
            $jadwal->created_at = Auth::id();
            $jadwal->save();
            

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }
    public function edit($id)
   {
    $jadwal = JadwalModel::findOrFail($id);
    return showResponseSuccess($jadwal);
    
   }

    public function update(Request $request, $id)
    {
        // 1 Admin App , 2 Admin Sekolah
   //     if (Auth::user()->role_id !== 1){
            // return abort(401); 
     //   }
        
        $validator = Validator::make($request->all(), [
            'tanggal' => 'required',
            'pelajaran' => 'required',
            'guru' => 'required',
            'jam1' => 'required',
            'jam2' => 'required',
        ]);
        // dd($request->all());
        if ($validator->passes()) {
            $jadwal = JadwalModel::findOrFail($id);
            $jadwal->tanggal = $request->tanggal;
            $jadwal->pelajaran = $request->pelajaran;
            $jadwal->guru = $request->guru;
            $jadwal->jam1 = $request->jam1;
            $jadwal->jam2 = $request->jam2;
            $jadwal->updated_at = Auth::id();
            $jadwal->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }

    public function destroy($id)
   {
        // 1 Admin App , 2 Admin Sekolah
    //    if (Auth::user()->role_id !== 1){
      //      return abort(401); 
       // }

        $jadwal = JadwalModel::findOrFail($id);
        $jadwal->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
    public function jabar(Request $request)
    {
        $pilih = $request->get('pilih');
        $nilai = $request->get('nilai');
        $dependent = $request->get('dependent');

        $data = DB::table('guru')
                ->where($pilih,$nilai)
                ->groupBy($dependent)
                ->get();
        $hasil = '<option value="">Pilih '.ucfirst($dependent).' Guru</option>';
        foreach ($data as $value){
            $hasil .= '<option value="'.$value->id.'">'.$value->$dependent.'</option>';
        }
        echo $hasil;
    }
}
