<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PelajaranModel;
use App\Models\GuruModel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use Image;
use DB;
use Illuminate\Support\Facades\Auth;

class GuruController extends Controller
{
    public function index()
    {
        $pelajaran = DB::table('pelajaran')
                    ->groupBy('pelajaran')
                    ->get();
        return view('guru.index', compact('pelajaran'));
    }
    public function tableguru()
    {
        $guru = GuruModel::with('pelajaran');
        // dd($siswa);
        return Datatables::of($guru)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nip' => 'required|max:255',
            'nama' => 'required|max:255',
            'nohp' => 'required|max:15',
            'pelajaran' => 'required|max:255',
            
            // 'avatar'=> 'required',
        ]);
        if ($validator->passes()) {

            $guru = new GuruModel;
            $guru->nip = $request->nip;
            $guru->nama = $request->nama;
            $guru->nohp = $request->nohp;
            $guru->pelajaran = $request->pelajaran;
            $guru->created_at = Auth::id();
            $guru->updated_at = Auth::id();
            $guru->save();
            // dd($siswa);

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }
    public function edit($id)
   {
    $guru = GuruModel::findOrFail($id);
    return showResponseSuccess($guru);
    
   }

    public function update(Request $request, $id)
    {
        // 1 Admin App , 2 Admin Sekolah
   //     if (Auth::user()->role_id !== 1){
            // return abort(401); 
     //   }
        
        $validator = Validator::make($request->all(), [
            'nip' => 'required|max:255',
            'nama' => 'required|max:255',
            'nohp' => 'required|max:15',
            'pelajaran' => 'required|max:255',
        ]);
        // dd($request->all());
        if ($validator->passes()) {
            
            
            $guru = GuruModel::findOrFail($id);
            // $siswa->update($request->all());
            $guru->nip = $request->nip;
            $guru->nama = $request->nama;
            $guru->nohp = $request->nohp;
            $guru->pelajaran = $request->pelajaran;
            $guru->updated_at = Auth::id();
            $guru->update();
            

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }

    public function destroy($id)
   {
        // 1 Admin App , 2 Admin Sekolah
    //    if (Auth::user()->role_id !== 1){
      //      return abort(401); 
       // }

        $guru = GuruModel::findOrFail($id);
        $guru->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
    public function profile(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $jurusan = Jurusan::all();
        $kelas = KelasModel::all();
        // dd($siswa);
        return view('siswa.profile',compact('siswa','kelas','jurusan'));
    }
    
}
