<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Jurusan;
use App\Models\KelasModel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use Image;
use Illuminate\Support\Facades\Auth;

class SiswaController extends Controller
{
    public function index()
    {
        $jurusan = Jurusan::all();
        $kelas = KelasModel::all();
        return view('siswa.index',compact('jurusan','kelas'));
    }
    public function tablesiswa()
    {
       
        $siswa = Siswa::with('jurusan','kelas');
        // dd($siswa);
        return Datatables::of($siswa)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'kelas' => 'required|max:255',
            'jurusan' => 'required|integer',
            'alamat' => 'required|max:255',
            'nama_ortu' => 'required|max:255',
            // 'avatar'=> 'required|image|mimes:jpg,png,jpeg|max:2048',
        ]);
        if ($validator->passes()) {
            // $avatar = $request->file('avatar');
            // $namaGambar = 'galeri-'.date('His').'.'.$avatar->getClientOriginalExtension();
            // $path = 'images/'.$namaGambar;
            // Image::make($avatar->getRealPath())->resize(400,250)->save($path);
            

            $siswa = new Siswa;
            $siswa->nama = $request->nama;
            $siswa->kelas = $request->kelas;
            $siswa->jurusan = $request->jurusan;
            $siswa->alamat = $request->alamat;
            $siswa->nama_ortu = $request->nama_ortu;
            // $siswa->avatar = $request->file('avatar');
            // $new_name = rand().'.'.$siswa->getClientOriginalExtension();
            // $siswa->move(public_patch('images'),$new_name);
            // if($request->hasfile('avatar')){
            //     $file = $request->file('avatar');
            //     $extension = $file->getOriginalExtension();
            //     $filename = time().'.'.$extension;
            //     $file->move('images',$filename);
            //     $siswa->avatar = $filename;
            // } else{
            //     return $request;
            //     $siswa->avatar = '';
            // }
            // dd($image);
            $siswa->save();
            

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }
    public function edit($id)
   {
    $siswa = Siswa::findOrFail($id);
    return showResponseSuccess($siswa);
    
   }

    public function update(Request $request, $id)
    {
        // 1 Admin App , 2 Admin Sekolah
   //     if (Auth::user()->role_id !== 1){
            // return abort(401); 
     //   }
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'kelas' => 'required|max:255',
            'jurusan' => 'required|integer',
            'alamat' => 'required|max:255',
            'nama_ortu' => 'required|max:255',
            // 'avatar' => 'required',
        ]);
        // dd($request->all());
        if ($validator->passes()) {
        //     $avatar = $request->file('avatar');
        //     $extension = $avatar->getClientOriginalName();
        //     $filename = time().'.'.$extension;
        //     $file->move('images', $filename);
            // $siswa->avatar = $filename
            
            $siswa = Siswa::findOrFail($id);
            // $siswa->update($request->all());
            $siswa->nama = $request->nama;
            $siswa->kelas = $request->kelas;
            $siswa->jurusan = $request->jurusan;
            $siswa->alamat = $request->alamat;
            $siswa->nama_ortu = $request->nama_ortu;
            $siswa->avatar = $request->avatar;
            $siswa->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }

    public function destroy($id)
   {
        // 1 Admin App , 2 Admin Sekolah
    //    if (Auth::user()->role_id !== 1){
      //      return abort(401); 
       // }

        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
    public function profile(Request $request, $id)
    {
        $siswa = Siswa::findOrFail($id);
        $jurusan = Jurusan::all();
        $kelas = KelasModel::all();
        // dd($siswa);
        return view('siswa.profile',compact('siswa','kelas','jurusan'));
    }
    
}
