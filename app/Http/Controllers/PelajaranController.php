<?php

namespace App\Http\Controllers;

use App\Models\PelajaranModel;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use DB;
use Illuminate\Support\Facades\Auth;

class PelajaranController extends Controller
{
    public function index()
    {
        $pelajaran = PelajaranModel::all();
        return view('guru.pelajaran.index');
    }
    public function tablejurusan()
    {
        $pelajaran = DB::table('pelajaran')
        ->groupBy('pelajaran')
        ->get();
        // dd($jurusan);
        return Datatables::of($pelajaran)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'pelajaran' => 'required|max:255',
        ]);
        if ($validator->passes()) {
            
            $pelajaran = new PelajaranModel;
            $pelajaran->pelajaran = $request->pelajaran;
            $pelajaran->created_at = Auth::id();
            $pelajaran->updated_at = Auth::id();
            $pelajaran->save();
            // dd($siswa);

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
        
    }
    public function edit($id)
   {
    $pelajaran = PelajaranModel::findOrFail($id);
    return showResponseSuccess($pelajaran);
    
   }

    public function update(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'pelajaran' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $pelajaran = PelajaranModel::findOrFail($id);
            $pelajaran->pelajaran = $request->pelajaran;        
            $pelajaran->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }

    public function destroy($id)
    {
        $pelajaran = PelajaranModel::findOrFail($id);
        $pelajaran->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
    

}

