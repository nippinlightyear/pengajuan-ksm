<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KelasModel;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Auth;


class KelasController extends Controller
{
    public function index()
    {
        $kelas = KelasModel::all();
        return view('siswa.kelas.index');
    }
    public function tablejurusan()
    {
        $kelas = KelasModel::all();
        // dd($jurusan);
        return Datatables::of($kelas)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'kelas' => 'required|max:255',
        ]);
        if ($validator->passes()) {
            
            $kelas = new KelasModel;
            $kelas->kelas = $request->kelas;
            $kelas->created_by = Auth::id();
            $kelas->updated_by = Auth::id();
            $kelas->save();
            // dd($siswa);

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }
    public function edit($id)
   {
    $kelas = KelasModel::findOrFail($id);
    return showResponseSuccess($kelas);
    
   }

    public function update(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'kelas' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $kelas = KelasModel::findOrFail($id);
            $kelas->kelas = $request->kelas;        
            $kelas->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }
    public function destroy($id)
    {
        $kelas = KelasModel::findOrFail($id);
        $kelas->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
}
