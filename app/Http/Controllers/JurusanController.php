<?php

namespace App\Http\Controllers;
use App\Models\Jurusan;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;
use Illuminate\Support\Facades\Auth;

class JurusanController extends Controller
{
    public function index()
    {
        $jurusan = Jurusan::all();
        return view('siswa.jurusan.index');
    }
    public function tablejurusan()
    {
        $jurusan = Jurusan::all();
        // dd($jurusan);
        return Datatables::of($jurusan)->make(true);
    }
    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'jurusan' => 'required|max:255',
        ]);
        if ($validator->passes()) {
            
            $jurusan = new Jurusan;
            $jurusan->jurusan = $request->jurusan;
            $jurusan->created_by = Auth::id();
            $jurusan->updated_by = Auth::id();
            $jurusan->save();
            // dd($siswa);

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
        dd($jurusan);
    }
    public function edit($id)
   {
    $jurusan = Jurusan::findOrFail($id);
    return showResponseSuccess($jurusan);
    
   }

    public function update(Request $request, $id)
    {   
        $validator = Validator::make($request->all(), [
            'jurusan' => 'required|max:255',
        ]);

        if ($validator->passes()) {

            $jurusan = Jurusan::findOrFail($id);
            $jurusan->jurusan = $request->jurusan;        
            $jurusan->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());
    }

    public function destroy($id)
    {
        $jurusan = Jurusan::findOrFail($id);
        $jurusan->delete();

        return showResponseSuccess(200, 'Data berhasil dihapus');
    }
    

}
