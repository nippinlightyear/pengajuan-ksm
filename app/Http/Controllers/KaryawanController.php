<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admins;
use App\Models\Roles;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Validator;
use Response;

class KaryawanController extends Controller
{
     public function index()
    {
        // 1 Admin App , 2 Admin Sekolah
        //if (Auth::user()->role_id !== 2){
            //return abort(401); 
    //}
        $roles = Roles::all();
        return view('karyawan.index', compact('roles'));
    }

    public function tablekaryawan()
    {
        $karyawan = Admins::all();

        return Datatables::of($karyawan)->make(true);
    }

   public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|max:255',
            'nip' => 'required|max:255',
            'jabatan' => 'required|max:255',
            'password' => 'required|max:255',
            'role_id' => 'required|max:255',
            
        ]);

        if ($validator->passes()) {

            $karyawan = new Admins;
            $karyawan->nama = $request->nama;
            $karyawan->email = $request->email;
           	$karyawan->nip = $request->nip;
           	$karyawan->jabatan = $request->jabatan;
           	$karyawan->password = app('hash')->make($request->password);
            $karyawan->role_id = $request->role_id;
            $karyawan->created_by = Auth::id();
            $karyawan->updated_by = Auth::id();
            $karyawan->save();

            return showResponseSuccess(200, 'Data berhasil ditambahkan');
        }
        return showResponseError(404, $validator->errors());
    }

    public function edit($id)
   {
    $karyawan = Admins::findOrFail($id);
    return showResponseSuccess($karyawan);
   }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required|max:255',
            'email' => 'required|max:255',
            'nip' => 'required|max:255',
            'jabatan' => 'required|max:255',
            'password' => 'required|max:255',
            'role_id' => 'required|max:255',
            

           
        ]);

        if ($validator->passes()) {

            $karyawan = Admins::findOrFail($id);


            $karyawan->nama = $request->nama;
            $karyawan->email = $request->email;
            $karyawan->nip = $request->nip;
           	$karyawan->jabatan = $request->jabatan;
            $karyawan->password = app('hash')->make($request->password);
            $karyawan->role_id = $request->role_id;
            $karyawan->updated_by = Auth::id(); 
            
            $karyawan->update();

            return showResponseSuccess(200, 'Data berhasil perbarui');
        }
        return showResponseError(404, $validator->errors());

    }

   public function deleteKaryawan(Request $req)
    {
        if (!$req->has(['id'])) {
            return showResponseError(400, 'Parameter ID tidak tersedia');
        }

        $karyawan = $req->only(['id']);

        $result = Admins::where($karyawan)->delete();

        if ($result) {
            return showResponseSuccess($result);
        } else {
            return showResponseError($result);
        }
    }

     public function store (Request $request)
    {
        dd($karyawan);
        $validator = Validator::make($request->all(),
            [
            // Data jabatan
            'nama' => 'required|max:255',
            'email' => 'required|max:255',
            'nip' => 'required|max:255',
            'jabatan' => 'required|max:255',
            'password' => 'required|max:255',
           
        ]);

        if ($validator->passes()) {

            DB::beginTransaction();
            try {
                

                $karyawan = new Admins;
                $karyawan->nama = $request->nama;
                $karyawan->email = $request->email;
                $karyawan->nip = $request->nip;
           		$karyawan->jabatan = $request->jabatan;
                $karyawan->password = app('hash')->make($request->password);
                $karyawan->role_id = $request->role_id;
                $karyawan->created_by = Auth::id();
                $karyawan->updated_by = Auth::id();
                //$siswa->created_by = Auth::id();
                //$siswa->updated_by = Auth::id();
               
                $karyawan->save();
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollback();
                return showResponseError(500, ['error' => $ex->getMessage()] );
            }

            return showResponseSuccess(200, 'Data berhasil di perbarui');
        }
        return showResponseError(404, $validator->errors());
    }
    //
}
