<?php

namespace App\Http\Controllers;
use App\Models\Training;
use Illuminate\Support\Facades\Auth;
use DB;
class HomeController extends BaseControllerAdmin
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $idKolektibiitas = Training::select('kolektibilitas')->distinct()->get();


        $kolektibilitas1 = Training::where('kolektibilitas', '=', '1')->count();
        $kolektibilitas2A = Training::where('kolektibilitas', '=', '2A')->count();
        $kolektibilitas2B = Training::where('kolektibilitas', '=', '2B')->count();
        $kolektibilitas2C = Training::where('kolektibilitas', '=', '2C')->count();
        $kolektibilitas3 = Training::where('kolektibilitas', '=', '3')->count();
        $kolektibilitas4 = Training::where('kolektibilitas', '=', '4')->count();
        $kolektibilitas5 = Training::where('kolektibilitas', '=', '5')->count();
        

        $dataKol = [
            '1' => $kolektibilitas1,
            '2A' => $kolektibilitas2A,
            '2B' => $kolektibilitas2B,
            '2C' => $kolektibilitas2C,
            '3' => $kolektibilitas3,
            '4' => $kolektibilitas4,
            '5' => $kolektibilitas5,

        ];
        //dd($dataKol);
        

        // foreach ($kolektibilitas as $idKol) {
        //     $allKol[] = DB::table('training')
        //      ->select(DB::raw('count(*) as data_count, kolektibilitas'))
        //      ->where('kolektibilitas', '=', $idKol->kolektibilitas)
        //      ->groupBy('id')
        //      ->get();
        // }
        
        //     dd($allKol);
    
        // salma
        // dd($idKolektibiitas->toArray());
        foreach ($idKolektibiitas as $idKol) {
        
            $users[] = DB::table('training')
             ->select(DB::raw('count(*) as data_count, kolektibilitas, nama_perusahaan'))
             ->where('kolektibilitas', '=', $idKol->kolektibilitas)
             ->groupBy('id')
             ->get();
        }
                     //dd($users);
        return view('home.index', compact('dataKol'));
    }
}
