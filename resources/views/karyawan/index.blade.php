@extends('layouts.app')

@section('title', 'Kelola Karyawan')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>List Karyawan</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahKaryawan()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="karyawanTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Nip</th>
                            <th>Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalKaryawan" tabindex="-1" role="dialog" aria-labelledby="modalJabatanLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmKaryawan">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="kode">Email</label>
                            <input type="text" name="email" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="email-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Nip</label>
                            <input type="text" name="nip" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nip-error"></strong>
                            </span>
                        </div> <div class="form-group">
                            <label for="kode">Jabatan</label>
                            <input type="text" name="jabatan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="jabatan-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Password</label>
                            <input type="password" name="password" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="password-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="role_id">Pilih Role</label>
                            <select name="role_id" class="form-control" required>
                            @foreach($roles as $role)
                                <option value="{!! $role->id !!}">{!! $role->nama !!}</option>
                            @endforeach
                            </select>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalKaryawan');
    var form = $('#frmKaryawan');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateKaryawan('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteKaryawan('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
    };

    var resetForm = function() {
        method = 'POST';
        $('[name="nama"]').val('');
        $('[name="email"]').val('');
        $('[name="nip"]').val('');
        $('[name="jabatan"]').val('');
        $('[name="password"]').val('');
        $('.modal-title').text('Tambah Data Karyawan');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahKaryawan = function() {
        formAction = '{!! route('karyawan.create') !!}';
        resetForm();
        frmModal.modal('show');
    }

    var updateKaryawan = function(id) {

        $.ajax({
            url: 'karyawan/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'karyawan/'+id+'/update';
                method = 'PUT';

                var data = resp.data;

                $('[name="nama"]').val(data.nama);
                $('[name="email"]').val(data.email);
                $('[name="nip"]').val(data.nip);
                $('[name="jabatan"]').val(data.jabatan);
                $('[name="password"]').val(data.password);
                $('[name="role_id"]').val(data.role_id);



                $('.modal-title').text('Edit Data Karyawan');
                frmModal.modal('show');
            },
            error: function(resp) {
                frmModal.modal('hide');
                error(resp);
            }
        })
    }

    var deleteKaryawan = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'karyawan/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }

    $(function(){

        table = $('#karyawanTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablekaryawan') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                { data: 'nama' },
                { data: 'email' },
                { data: 'nip' },
                { data: 'jabatan' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
        });

        $('.btn-simpan').click(function(e) {
            e.preventDefault();

            var data = form.serialize();

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                dataType: 'json',
                success: function(resp) {
                     done(resp);    
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                error: function(resp) {
                    if (resp.responseJSON.meta.message.nama){
                        $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    }
                    if(resp.responseJSON.meta.message.kode){
                        $( '#email-error' ).html( resp.responseJSON.meta.message.email[0] );
                    }
                            }
            });
            
            return false;
        });
    });
</script>
@endpush