@extends('layouts.app')

@section('title', 'Kelola Data Siswa')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Jadwal Pelajaran</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahJadwal()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered table-hover" id="jadwalTable">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Pelajaran</th>
                            <th>Guru</th>
                            <th>Jam</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalJadwal" tabindex="-1" role="dialog" aria-labelledby="modalJadwallable">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmJadwal">
                    <div class="modal-body">
                        {{ csrf_field() }}
            
                        <div class="form-group">
                            <label for="nama">Tanggal</label>
                            <input type="text" name="tanggal" class="form-control datepicker" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div> 
                        <div class="form-group">
                            <label for="role_id">Pelajaran</label>
                            <select name="pelajaran" class="form-control dinamis" id="pelajaran" data-dependent="nama" required>
                            <option value="">Pilih Pelajaran</option>                            
                            @foreach($pelajaran as $pljrn)
                                <option value="{!! $pljrn->id !!}">{!! $pljrn->pelajaran !!}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role_id">Guru</label>
                            <select name="guru" class="form-control" id="nama" required>
                                <option value="">Pilih Guru</option>
                            </select>
                        </div>    
                        <div class="form-group">
                            <label for="kode">Jam</label>
                            <input type="clock" name="jam1" class="clockpicker" max-length="3" value="00:00:00">
                        </div>
                        <div class="form-group">
                            <label for="kode">s/d</label>
                            <input type="clock" name="jam2" class="clockpicker" max-length="3" value="00:00:00">
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalJadwal');
    var form = $('#frmJadwal');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateJadwal('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteJadwal('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
        text: function(row, type, data){
            return 'Mulai dari '+data.jam1+' sampai dengan '+data.jam2;
        },
    };
    $(document).ready(function(){
        $('input.datepicker').datepicker({});
        $('input.timepicker').timepicker({
            
        });
    });
    $(document).ready(function(){
        $('.dinamis').change(function(){
            if($(this).val() != ''){
                var pilih = $(this).attr('id')
                var nilai = $(this).val()
                // console.log(nilai);
                var dependent = $(this).data('dependent')
                var _token = $("input[name ='_token']").val();
                $.ajax({
                    url: '{!! Route('jabar')!!}',
                    method: "POST",
                    data: { pilih: pilih, nilai: nilai, dependent: dependent, _token: _token},
                    success: function(result){
                        $('#'+dependent).html(result);
                    }
                })
            }
        }) 
    })
    var resetForm = function() {
        method = 'POST';
        $('[name="tanggal"]').val('');
        $('[name="pelajaran"]').val('');
        $('[name="guru"]').val('');
        $('[name="jam1"]').val('');
        $('[name="jam2"]').val('');
        $('.modal-title').text('Tambah Jadwal Pelajaran');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahJadwal = function() {
        formAction = '{!! route('jadwal.create') !!}';
        resetForm();
        frmModal.modal('show');
    }

    var updateJadwal = function(id) {
        $.ajax({
            url: 'jadwal/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'jadwal/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="tanggal"]').val(data.tanggal);
                $('[name="pelajaran"]').val(data.pelajaran);
                // $('[name="guru"]').val(data.guru);
                $('[name="jam1"]').val(data.jam1);
                $('[name="jam2"]').val(data.jam2);
                // $('[name="avatar"]').val(data.avatar);
                // console.log()

                $('.modal-title').text('Edit Data Siswa');
                frmModal.modal('show');
            },
            error: function(resp) {
                frmModal.modal('show');
                error(resp);
            }
        })
    }

    var deleteJadwal = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'jadwal/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }
    $(function(){   
        table = $('#jadwalTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablejadwal') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'tanggal' },
                { data: 'pelajaran.pelajaran' },
                { data: 'guru.nama' },
                { data: 'id', render: styles.text },
                
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
            
        });
        
        $('.btn-simpan').click(function(e) {
            e.preventDefault();
            // console.log(avatar)
            var data = form.serialize();
            
            $( '#nama-error' ).html( "" );
            $( '#kelas-error' ).html( "" );
            $( '#jurusan-error' ).html( "" );

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                success: function(resp) {
                    done(resp);
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                
                error: function(resp) {
                    if (resp.responseJSON.meta.message.nama){
                        $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    }
                    if(resp.responseJSON.meta.message.kelas){
                        $( '#kelas-error' ).html( resp.responseJSON.meta.message.kelas[0] );
                    }
                    if(resp.responseJSON.meta.message.jurusan){
                    $( '#jurusan-error' ).html( resp.responseJSON.meta.message.jurusan[0] );   
                    }
                }
            });
            
            return false;
        });
    });
</script>
@endpush