@extends('layouts.app')

@section('title', 'Main page')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center m-t-lg">
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                    <div class="col-lg-3">
                        <div class="widget blue-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 1</h4>
                        <div class="ibox">{{$dataKol['1']}}</div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget red-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 2A</h4>
                        <div class="ibox">{{$dataKol['2A']}}</div>
                    
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget yellow-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 2B</h4>
                        <div class="ibox">{{$dataKol['2B']}}</div>
                        
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget blue-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 2C</h4>
                        <div class="ibox">{{$dataKol['2C']}}</div>
                     
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget navy-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 3</h4>
                        <div class="ibox">{{$dataKol['3']}}</div>
                       
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget yellow-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 4</h4>
                        <div class="ibox">{{$dataKol['4']}}</div>
                      
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="widget red-bg p-lg ">
                            <h4>Nasabah Kolektibilitas 5</h4>
                        <div class="ibox">{{$dataKol['5']}}</div>
                        
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@stop

@push('scripts')
<!-- Custom and plugin javascript -->
<script src="{!! asset('js/inspinia.js') !!}"></script>
<script src="{!! asset('js/pace.min.js') !!}"></script>
@endpush
