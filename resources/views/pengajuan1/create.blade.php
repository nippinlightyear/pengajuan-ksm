@extends('layouts.app')

@section('title', 'Kelola Pengajuan')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tambah Data Pengajuan</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Form Tambah Data Pengajuan</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                     <div class="ibox-content">
                      

                        <form id="frmAddPengajuan" method="post" name="hitunggaji">
                             {{ csrf_field() }}
                             

                        <div class="form-group">
                            <label for="kode">Nama Lengkap</label>
                            <input type="text" name="nama" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">No.KTP</label>
                            <input type="text" name="nik" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nik-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="alamat-error"></strong>
                            </span>
                        </div>
                                                    
                           <div class="form-group">
                            <label for="kode">Nama Perusahaan</label>
                            <input type="text" name="nama_perusahaan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_perusahaan-error"></strong>
                            </span>
                        </div>
                       

                        <div class="form-group">
                            <label for="kode">Gaji</label>
                            <input type="text" name="gaji" class="form-control">
                            <span class="text-danger">
                                <strong id="gaji-error"></strong>
                            </span>
                        </div>

                         <div class="form-group">
                            <label for="kode">Tunjangan Kerja</label>
                            <input type="text" name="tunjangan_kerja" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="tunjangan_kerja-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Insentif</label>
                            <input type="text" name="insentif" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="insentif-error"></strong>
                            </span>
                        </div>
                       
                         <div class="form-group">
                            <label for="kode">Limit</label>
                            <input type="text" name="limit" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="limit-error"></strong>
                            </span>
                        </div>

                         <div class="form-group">
                            <label for="nama">Tenor</label>
                            <input type="text" name="tenor" class="form-control">
                            <span class="text-danger">
                                <strong id="tenor-error"></strong>
                            </span>
                        </div>


                        <div class="form-group">
                            <label for="kode">Bunga</label>
                            <input type="text" name="bunga" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="bunga-error"></strong>
                            </span>
                        </div>
                   

                        <div class="form-group">
                            <label for="nama">Kolektibilitas</label>
                            <input type="text" name="kolektibilitas" class="form-control" required>
                            <span class="text-danger">
                                <strong id="kolektibilitas-error"></strong>
                            </span>
                        </div>


                        <br><input type="button" onclick="hitung()" value="check" class="btn btn-warning"></input>
                        <!--<input type="reset" type="reset"></input>-->

                         <!-- <div class="form-group">
                            <label for="dsr">Dsr</label>
                            <input type="text" name="dsr" class="form-control" disabled="true">
                            <span class="text-danger">
                                <strong id="dsr-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Angsuran</label>
                            <input type="text" name="angsuran" class="form-control">
                            <span class="text-danger">
                                <strong id="angsuran-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Status</label>
                            <input type="text" name="status" class="form-control">
                            <span class="text-danger">
                                <strong id="status-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 01</label>
                            <input type="text" name="nk1" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 2A</label>
                            <input type="text" name="nk2a" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 2B</label>
                            <input type="text" name="nk2b" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 2C</label>
                            <input type="text" name="nk2c" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 03</label>
                            <input type="text" name="nk3" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 04</label>
                            <input type="text" name="nk4" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Nilai Kolektibilitas 05</label>
                            <input type="text" name="nk5" class="form-control">
                        </div> -->

                        </form>
                   

                       
                        
                            <!-- <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-white btn-sm" type="submit">Cancel</button>
                                    <button class="btn btn-primary btn-simpan" type="submit">Simpan</button>
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPengajuan" tabindex="-1" role="dialog" aria-labelledby="modalJabatanLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmKaryawan">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" disabled="true" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="kode">Jenis Perusahaan</label>
                            <input type="text" name="jenis_perusahaan" class="form-control" disabled="true">
                            <span class="text-danger">
                                <strong id="jenis_perusahaan-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Limit</label>
                            <input type="text" name="limit" class="form-control" disabled="true" >
                            <span class="text-danger">
                                <strong id="limit-error"></strong>
                            </span>
                        </div> <div class="form-group">
                            <label for="kode">Status</label>
                            <input type="text" name="status" class="form-control" disabled="true">
                            <span class="text-danger">
                                <strong id="status-error"></strong>
                            </span>
                        </div>
                         
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    var mem = $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    
    var frmModal = $('#modalPengajuan');
    var form = $('#frmAddPengajuan');
    
    var done = function(resp) {
        // frmModal.modal('hide');
        // table.ajax.reload();
        $("#frmAddPengajuan")[0].reset();
    }

    function hitung() {
            var nama = document.hitunggaji.nama.value;
            var nama_perusahaan = document.hitunggaji.nama_perusahaan.value;
            var gaji= parseFloat(document.hitunggaji.gaji.value);  
            if (isNaN(gaji)) {gaji=0.0;}  
            var tunjangan_kerja=parseFloat(document.hitunggaji.tunjangan_kerja.value);  
            if (isNaN(tunjangan_kerja)) {tunjangan_kerja=0.0;}
            var insentif=parseFloat(document.hitunggaji.insentif.value);  
            if (isNaN(insentif)) {insentif=0.0;}
            var limit=parseFloat(document.hitunggaji.limit.value);  
            if (isNaN(limit)) {limit=0.0;}
            var tenor=parseFloat(document.hitunggaji.tenor.value);  
            if (isNaN(tenor)) {tenor=0.0;}
            var bunga=parseFloat(document.hitunggaji.bunga.value);  
            if (isNaN(bunga)) {bunga=0.0;}

            var gajitunjangan_kerja= ((((gaji * 100)/100) + ((tunjangan_kerja *80)/100) + ((insentif *80)/100)) *60 )/100 ;

            var hasil = (((limit * 10) / 100) + limit) / tenor;


            var hasil_status =gajitunjangan_kerja > hasil;

            var nilai_kolektibilitas1 = ({{$dataKol['01np']}} / {{$dataKol['01']}}) * ({{$dataKol['01dl']}} / {{$dataKol['01']}}) * ({{$dataKol['01t']}} / {{$dataKol['01']}});

            var nilai_kolektibilitas2a = ({{$dataKol['2Anp']}} / {{$dataKol['2A']}}) * ({{$dataKol['2Adl']}} / {{$dataKol['2A']}}) * ({{$dataKol['2At']}} / {{$dataKol['2A']}});

            var nilai_kolektibilitas2b = ({{$dataKol['2Bnp']}} / {{$dataKol['2B']}}) * ({{$dataKol['2Bdl']}} / {{$dataKol['2B']}}) * ({{$dataKol['2Bt']}} / {{$dataKol['2B']}});

            var nilai_kolektibilitas2c = ({{$dataKol['2Cnp']}} / {{$dataKol['2C']}}) * ({{$dataKol['2Cdl']}} / {{$dataKol['2C']}}) * ({{$dataKol['2Ct']}} / {{$dataKol['2C']}});

            var nilai_kolektibilitas3 = ({{$dataKol['03np']}} / {{$dataKol['03']}}) * ({{$dataKol['03dl']}} / {{$dataKol['03']}}) * ({{$dataKol['03t']}} / {{$dataKol['03']}});

            var nilai_kolektibilitas4 = ({{$dataKol['04np']}} / {{$dataKol['04']}}) * ({{$dataKol['04dl']}} / {{$dataKol['04']}}) * ({{$dataKol['04t']}} / {{$dataKol['04']}});

            var nilai_kolektibilitas5 = ({{$dataKol['05np']}} / {{$dataKol['05']}}) * ({{$dataKol['05dl']}} / {{$dataKol['05']}}) * ({{$dataKol['05t']}} / {{$dataKol['05']}});            

            
            // document.hitunggaji.angsuran.value= hasil;
            // document.hitunggaji.dsr.value= gajitunjangan_kerja; 

            var status = "";
            if (hasil_status) {
                status = "Lancar";
            } else {
                status = "Tidak Lancar";
            }

            var jenis_perusahaan = "";
            if (nama_perusahaan == "Sanbe Farma, PT - Cimahi" || "BANK MANDIRI (PERSERO) TBK." || "POS INDONESIA (PERSERO) - KPRK BANDUNG" || "PLN (PERSERO)" || "TASPEN" || "DANA PENSIUN TELKOM" || "BADAN PENGAWAS KEUANGAN" || "KIMIA FARMA, TBK" || "AXA MANDIRI FINANCIAL SERVICES" || "Telkom" || "BANK MANDIRI, PT," || "INTI" || "Koperasi Karyawan Garuda Mandiri" || "Asuransi Jiwa Inhealth Indonesia" || "PEKAS TNI" || "BANK MANDIRI" || "Telekomunikasi Indonesia (Telkom)" || "Telekomunikasi Indonesia (Telkom), PT" || "YPT, (YAYASAN PENDIDIKAN TELKOM) - Bandung" || "TELKOM") {
                jenis_perusahaan = "BUMN";
            } else if (nama_perusahaan == "Dinas Sosial Kota Bandung" || "PUSAT PENDIDIKAN INTELEJEN KEAMANAN LEMBAGA PENDIDIKAN DAN PELATIHAN KEPOLISIAN" || "KEMENTERIAN KEUANGAN" || "Kanwil Direktorat Jendral Pajak Jawa Barat I" || "Direktorat Keuangan Angkatan Darat Keuangan Pusat II" || "KEMENTERIAN KEUANGAN REPUBLIK INDONESIA" || "RUMAH SAKIT IBU DAN ANAK SUKAJADI" || "KEMENTERIAN PERTAHANAN" || "Perkebunan Nusantara VIII (PTPN VIII)") {
                jenis_perusahaan = "PNS";
            } else {
                jenis_perusahaan = "Swasta";
            }
            // document.hitunggaji.nk1.value = nilai_kolektibilitas1;
            // document.hitunggaji.nk2a.value = nilai_kolektibilitas2a;
            // document.hitunggaji.nk2b.value = nilai_kolektibilitas2b;
            // document.hitunggaji.nk2c.value = nilai_kolektibilitas2c;
            // document.hitunggaji.nk3.value = nilai_kolektibilitas3;
            // document.hitunggaji.nk4.value = nilai_kolektibilitas4;
            // document.hitunggaji.nk5.value = nilai_kolektibilitas5;

            method = 'POST';
            $('[name="nama"]').val(nama);
            $('[name="jenis_perusahaan"]').val(jenis_perusahaan);
            $('[name="limit"]').val(limit);
            $('[name="status"]').val(status);
            $('.modal-title').text('Alert');
            frmModal.modal('show');
    }

    $(function(){

    $('.btn-simpan').click(function(e) {
        e.preventDefault();

            var data = form.serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: "{{ route('pengajuan.store')}}",
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(resp) {
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                     done(resp);
                     frmModal.modal('hide');
                },
                error: function(resp) {
                    if (resp.responseJSON.meta.message.agama){
                        $( '#agama-error' ).html( resp.responseJSON.meta.message.agama[0] );
                    }                    
                    if (resp.responseJSON.meta.message.alamat){
                        $( '#alamat-error' ).html( resp.responseJSON.meta.message.alamat[0] );
                    }
                   
                }
            });
            
            return false;
        });
    });

    
     
</script>
@endpush