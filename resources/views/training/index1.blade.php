@extends('layouts.app')

@section('title', 'Kelola Training')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>List Training</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <!-- <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahTraining()"><i class="fa fa-plus"></i></a> -->
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="importExcel()"><i class="fa fa-cloud-upload"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="trainingTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <!-- <th>NO Rekening</th> -->
                            <th>Nama Nasabah</th>
                            <th>Alamat</th>
                            <th>No Tlp</th>
                            <th>Nama Perusahaan</th>
                            <th>Limit</th>
                            <th>Kolektibilitas</th>
                            <th>Tenor</th>
                            <th>Penghasilan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalTraining" tabindex="-1" role="dialog" aria-labelledby="modalTrainingLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmTraining">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">No Rekening</label>
                            <input type="text" name="no_rek" class="form-control" required>
                            <span class="text-danger">
                                <strong id="no_rek-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="kode">Nama Nasabah</label>
                            <input type="text" name="nama_nasabah" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_nasabah-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="alamat-error"></strong>
                            </span>
                        </div> <div class="form-group">
                            <label for="kode">No Telp</label>
                            <input type="text" name="no_tlp" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="no_tlp-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Nama Perusahaan</label>
                            <input type="text" name="nama_perusahaan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_perusahaan-error"></strong>
                            </span>
                        </div>
                       
                        <div class="form-group">
                            <label for="kode">Limit</label>
                            <input type="text" name="data_limit" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="limit-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="kode">Kolektibilitas</label>
                            <input type="text" name="kolektibilitas" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="kolektibilitas-error"></strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <label for="kode">Tenor</label>
                            <input type="text" name="tenor" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="tenor-error"></strong>
                            </span>
                        </div>
                         <div class="form-group">
                            <label for="kode">Penghasilan</label>
                            <input type="text" name="penghasilan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="penghasilan-error"></strong>
                            </span>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Upload -->
    <div class="modal fade" id="modalImportTraining" tabindex="-1" role="dialog" aria-labelledby="modalImportTraining">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Training</h4>
                </div>
                <form id="frmImportTraining" action="{{ route('training.importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="modal-body" align="center">
                        {{ csrf_field() }}
                        <br>
                        <!-- <br> -->
                        <div class="form-group">
                            <input type="file" name="import_file">
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-primary">Import File</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalTraining');
    var form = $('#frmTraining');
    var modalImportTraining = $('#modalImportTraining');
    var frmImportTraining = $('#frmImportTraining');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" class="btn btn-danger btn-sm" onclick="deleteTraining('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
    };

    var resetForm = function() {
        method = 'POST';
        $('[name="no_rek"]').val('');
        $('[name="nama_nasabah"]').val('');
        $('[name="alamat"]').val('');
        $('[name="no_tlp"]').val('');
        $('[name="nama_perusahaan"]').val('');
        $('[name="data_limit"]').val('');
        $('[name="kolektibilitas"]').val('');
        $('[name="tenor"]').val('');
        $('[name="penghasilan"]').val('');
        $('.modal-title').text('Tambah Data training');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahTraining = function() {
        formAction = '{!! route('training.create') !!}';
        resetForm();
        frmModal.modal('show');
    }

    var updateTraining = function(id) {

        $.ajax({
            url: 'training/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'training/'+id+'/update';
                method = 'PUT';

                var data = resp.data;

                $('[name="no_rek"]').val(data.no_rek);
                $('[name="nama_nasabah"]').val(data.nama_nasabah);
                $('[name="alamat"]').val(data.alamat);
                $('[name="no_tlp"]').val(data.no_tlp);
                $('[name="nama_perusahaan"]').val(data.nama_perusahaan);
                $('[name="data_limit"]').val(data.data_limit);
                $('[name="kolektibilitas"]').val(data.kolektibilitas);
                $('[name="tenor"]').val(data.tenor);
                $('[name="penghasilan"]').val(data.penghasilan);

                $('.modal-title').text('Edit Data training');
                frmModal.modal('show');
            },
            error: function(resp) {
                frmModal.modal('hide');
                error(resp);
            }
        })
    }

    var deleteTraining = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'training/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }
    var importExcel = function() {
        $('#frmImportTraining').trigger("reset");
        modalImportTraining.modal('show');
    }


    $(function(){

        table = $('#trainingTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tabletraining') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                //{ data: 'no_rek' },
                { data: 'nama_nasabah' },
                { data: 'alamat' },
                { data: 'no_tlp' },
                { data: 'nama_perusahaan' },
                { data: 'data_limit' },
                { data: 'kolektibilitas' },
                { data: 'tenor' },
                { data: 'penghasilan' },
                //{ data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
        });

        $('.btn-simpan').click(function(e) {
            e.preventDefault();

            var data = form.serialize();

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                dataType: 'json',
                success: function(resp) {
                     done(resp);    
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                error: function(resp) {
                    if (resp.responseJSON.meta.message.no_rek){
                        $( '#no_rek-error' ).html( resp.responseJSON.meta.message.no_rek[0] );
                    }
                    if(resp.responseJSON.meta.message.kode){
                        $( '#nama_nasabah-error' ).html( resp.responseJSON.meta.message.nama_nasabah[0] );
                    }
                            }
            });
            
            return false;
        });
    });
</script>
@endpush