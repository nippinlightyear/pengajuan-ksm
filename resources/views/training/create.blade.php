@extends('layouts.app')

@section('title', 'Kelola Training')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tambah Data Training</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Form Tambah Data Training</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form id="frmAddTraining">
                            {{ csrf_field() }}
                             
                        <div class="form-group">
                            <label for="kode">No Rekening</label>
                            <input type="text" name="no_rek" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="no_rek-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Nama Nasabah</label>
                            <input type="text" name="nama_nasabah" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_nasabah-error"></strong>
                            </span>
                        </div>
                                                    
                           

                         <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="alamat-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">No Telpon</label>
                            <input type="text" name="no_tlp" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="no_tlp-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Nama Perusahaan</label>
                            <input type="text" name="nama_perusahaan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_perusahaan-error"></strong>
                            </span>
                        </div>

                       
                         <div class="form-group">
                            <label for="kode">Limit</label>
                            <input type="text" name="limit" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="limit-error"></strong>
                            </span>
                        </div>

                        
                       
                        <div class="form-group">
                            <label for="nama">Tenor</label>
                            <input type="text" name="tenor" class="form-control" required>
                            <span class="text-danger">
                                <strong id="tenor-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="nama">Kolektibilitas</label>
                            <input type="text" name="kolektibilitas" class="form-control" required>
                            <span class="text-danger">
                                <strong id="kolektibilitas-error"></strong>
                            </span>
                        </div>


                    

                        

                       
                        
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                       <!--              <button class="btn btn-white btn-sm" type="submit">Cancel</button> -->
                                    <button class="btn btn-primary btn-simpan" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    var mem = $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    
    var form = $('#frmAddTraining');
    
    var done = function(resp) {
        // frmModal.modal('hide');
        // table.ajax.reload();
        $("#frmAddTraining")[0].reset();
    }

    $(function(){

    $('.btn-simpan').click(function(e) {
        e.preventDefault();

            var data = form.serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: "{{ route('training.store')}}",
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(resp) {
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                     done(resp);
                },
                error: function(resp) {
                    if (resp.responseJSON.meta.message.agama){
                        $( '#agama-error' ).html( resp.responseJSON.meta.message.agama[0] );
                    }                    
                    if (resp.responseJSON.meta.message.alamat){
                        $( '#alamat-error' ).html( resp.responseJSON.meta.message.alamat[0] );
                    }
                   
                }
            });
            
            return false;
        });
    });

    
     
</script>
@endpush