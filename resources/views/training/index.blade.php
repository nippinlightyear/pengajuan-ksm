@extends('layouts.app')

@section('title', 'Kelola Training')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data Training</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <!-- <a href="{{ route('training.create')}}" class="btn btn-sm btn-primary" ><i class="fa fa-plus"></i></a> -->
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="importExcel()"><i class="fa fa-cloud-upload"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="trainingTable">
                    <thead>
                        <tr>
                           <th>ID</th>
                           <!-- <th>No Rekening</th> -->
                            <th>Nama Nasabah</th>
                            <th>Alamat</th>
                            <th>No Telpon</th>
                            <th>Nama Perusahaan</th>
                            <th>Limit</th>
                            <th>Tenor</th>
                            <th>Kolektibilitas</th>
                            <th>Aksi</th>
                            
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
<!-- Upload -->
    <div class="modal fade" id="modalImportTraining" tabindex="-1" role="dialog" aria-labelledby="modalImportTraining">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Import Training</h4>
                </div>
                <form id="frmImportTraining" action="{{ route('training.importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                    <div class="modal-body" align="center">
                        {{ csrf_field() }}
                        <br>
                        <!-- <br> -->
                        <div class="form-group">
                            <input type="file" name="import_file">
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <button class="btn btn-primary">Import File</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@stop

@push('scripts')
<script src="http://malsup.github.io/min/jquery.form.min.js"></script>
<script src="https://cdn.rawgit.com/fengyuanchen/cropper/v1.0.0/dist/cropper.min.js"></script>
<script>
    var frmModal = $('#modalTraining');
    var form = $('#frmTraining');
    var modalImportTraining = $('#modalImportTraining');
    var frmImportTraining = $('#frmImportTraining');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateUser('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteUser('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
    };

    var done = function(resp) {
        table.ajax.reload();
    }

    var deleteFormulir = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: '/training/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }
    var importExcel = function() {
        $('#frmImportTraining').trigger("reset");
        modalImportTraining.modal('show');
    }

    $(function(){

        table = $('#trainingTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tabletraining') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                // { data: 'no_rek' },
                { data: 'nama_nasabah' },
                { data: 'alamat' },
                { data: 'no_tlp' },
                { data: 'nama_perusahaan' },
                { data: 'data_limit' },
                { data: 'tenor' },
                { data: 'kolektibilitas' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
                
        
            ]
        });
    });
</script>
@endpush