@extends('layouts.app')

@section('title', 'Kelola Data Jurusan')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>List Guru</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahGuru()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered table-hover" id="guruTable">
                    <thead>
                        <tr>
                            <th>NIP</th>
                            <th>Nama Guru</th>
                            <th>Nomer Telepon</th>
                            <th>Mengajar Pelajaran</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalGuru" tabindex="-1" role="dialog" aria-labelledby="modalGuruLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmGuru">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">NIP</label>
                            <input type="text" name="nip" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nip-error"></strong>
                            </span>
                        </div> 
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div> 
                        <div class="form-group">
                            <label for="role_id">Nomer Telepon</label>
                            <input type="text" name="nohp" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="role_id">Mengajar Pelajaran</label>
                            <select name="pelajaran" class="form-control" required>
                            @foreach($pelajaran as $pljrn)
                                <option value="{!! $pljrn->id !!}">{!! $pljrn->pelajaran !!}</option>
                            @endforeach
                            </select>
                        </div>    
                        
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalGuru');
    var form = $('#frmGuru');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateGuru('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteGuru('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
        nama: function(row, type, data){
            return '<a href="/guru/'+data.id+'/profile">'+data.nama+'</a>';
        },
    };

    var resetForm = function() {
        method = 'POST';
        $('[name="nip"]').val('');
        $('[name="nama"]').val('');
        $('[name="nohp"]').val('');
        $('[name="pelajaran"]').val('');
        $('.modal-title').text('Tambah Data Siswa');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahGuru = function() {
        formAction = '{!! route('guru.create') !!}';
        resetForm();
        frmModal.modal('show');
    }

    var updateGuru = function(id) {
        $.ajax({
            url: 'guru/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'guru/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="nip"]').val(data.nip);
                $('[name="nama"]').val(data.nama);
                $('[name="nohp"]').val(data.nohp);
                $('[name="pelajaran"]').val(data.pelajaran);
                // $('[name="avatar"]').val(data.avatar);
                // console.log()

                $('.modal-title').text('Edit Data Guru');
                frmModal.modal('show');
            },
            error: function(resp) {
                frmModal.modal('show');
                error(resp);
            }
        })
    }

    var deleteGuru = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'guru/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }

    $(function(){   
        table = $('#guruTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tableguru') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'nip' },
                { data: 'id', render: styles.nama },
                { data: 'nohp' },
                { data: 'pelajaran.pelajaran' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
            
        });
        
        $('.btn-simpan').click(function(e) {
            e.preventDefault();
            // console.log(avatar)
            var data = form.serialize();
            $.ajax({
                url: formAction,
                method: method,
                data: data,
                success: function(resp) {
                    done(resp);
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                
                error: function(resp) {
                    if (resp.responseJSON.meta.message.nama){
                        $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    }
                    
                }
            });
            
            return false;
        });
    });
</script>
@endpush