@extends('layouts.app')

@section('title', 'Kelola Data Jurusan')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>List Pelajaran</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahPelajaran()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="pelajaranTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Pelajaran</th>
                            <th>Aksi</th>
                            
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPelajaran" tabindex="-1" role="dialog" aria-labelledby="modalPelajaranLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmPelajaran">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Nama Pelajaran</label>
                            <input type="text" name="pelajaran" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalPelajaran');
    var form = $('#frmPelajaran');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updatePelajaran('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deletePelajaran('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
    };
    var resetForm = function() {
        method = 'POST';
        $('[name="pelajaran"]').val('');
        $('.modal-title').text('Tambah Data Pelajaran');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahPelajaran = function() {
        formAction = '{!! route('pelajaran.create') !!}';
        resetForm();
        frmModal.modal('show');
    }
    var updatePelajaran = function(id) {
 
        $.ajax({
            url: 'pelajaran/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'pelajaran/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="pelajaran"]').val(data.pelajaran);
                $('.modal-title').text('Edit Data Pelajaran');
                frmModal.modal('show');
                },
            error: function(resp) {
                frmModal.modal('show');
                error(resp);
            }
        })
        }

        var deletePelajaran = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'pelajaran/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }
    $(function(){
            table = $('#pelajaranTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablepelajaran') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                { data: 'pelajaran' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
        });
        $('.btn-simpan').click(function(e) {
            e.preventDefault();

            var data = form.serialize();
            
            
            $( '#jurusan-error' ).html( "" );

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                success: function(resp) {
                    done(resp);
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                error: function(resp) {
                    // if (resp.responseJSON.meta.message.nama){
                    //     $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    // }
                }
            });
            
            return false;
    });
    });

</script>
@endpush