@extends('layouts.app')

@section('title', 'Kelola Pengajuan')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Data Pengajuan</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="{{ route('pengajuan.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="pengajuanTable">
                    <thead>
                        <tr>
                           <th>ID</th>
                            <th>Nama</th>
                            <th>No.KTP</th>
                            <th>Alamat</th>
                            <th>Nama Perusahaan</th>
                            <th>Gaji</th>
                            <th>Limit</th>
                            <th>Tenor</th>
                            <th>Kolektibilitas</th>
                            <th>Status</th>
                            

                            
                            
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    
@stop

@push('scripts')
<script>
    var frmModal = $('#modalPengajuan');
    var form = $('#frmPengajuan');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal"  onclick="deleteFormulir('+data.id+')"><i class="fa fa-trash"></i></a>';
        },

     
    };

    var done = function(resp) {
        table.ajax.reload();
    }

    var deleteFormulir = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: '/pengajuan/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }

    $(function(){

        table = $('#pengajuanTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablepengajuan') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                { data: 'nama' },
                { data: 'nik' },
                { data: 'alamat' },
                { data: 'nama_perusahaan' },
                { data: 'gaji' },
                { data: 'limit' },
                { data: 'tenor' },
                { data: 'kolektibilitas' },
                { data: 'status' }
                
        
            ]
        });
    });
</script>
@endpush