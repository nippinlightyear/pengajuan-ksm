@extends('layouts.app')

@section('title', 'Kelola Pengajuan')
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Tambah Data Pengajuan</h2>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Form Tambah Data Pengajuan</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#" class="dropdown-item">Config option 1</a>
                                </li>
                                <li><a href="#" class="dropdown-item">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                     <div class="ibox-content">
                      

                        <form id="frmAddPengajuan" method="post" name="hitunggaji">
                             {{ csrf_field() }}
                             

                        <div class="form-group">
                            <label for="kode">Nama</label>
                            <input type="text" name="nama" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">No.KTP</label>
                            <input type="text" name="nik" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nik-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="alamat-error"></strong>
                            </span>
                        </div>
                                                    
                           <div class="form-group">
                            <label for="kode">Nama Perusahaan</label>
                            <input type="text" name="nama_perusahaan" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="nama_perusahaan-error"></strong>
                            </span>
                        </div>
                       

                        <div class="form-group">
                            <label for="kode">Gaji</label>
                            <input type="text" name="gaji" class="form-control">
                            <span class="text-danger">
                                <strong id="gaji-error"></strong>
                            </span>
                        </div>

                         <div class="form-group">
                            <label for="kode">Tunjangan Kerja</label>
                            <input type="text" name="tunjangan_kerja" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="tunjangan_kerja-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="kode">Insentif</label>
                            <input type="text" name="insentif" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="insentif-error"></strong>
                            </span>
                        </div>
                       
                         <div class="form-group">
                            <label for="kode">Limit</label>
                            <input type="text" name="limit" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="limit-error"></strong>
                            </span>
                        </div>

                         <div class="form-group">
                            <label for="nama">Tenor</label>
                            <input type="text" name="tenor" class="form-control">
                            <span class="text-danger">
                                <strong id="tenor-error"></strong>
                            </span>
                        </div>


                        <div class="form-group">
                            <label for="kode">Bunga</label>
                            <input type="text" name="bunga" class="form-control" max-length="3">
                            <span class="text-danger">
                                <strong id="bunga-error"></strong>
                            </span>
                        </div>
                   

                        <div class="form-group">
                            <label for="nama">Kolektibilitas</label>
                            <input type="text" name="kolektibilitas" class="form-control" required>
                            <span class="text-danger">
                                <strong id="kolektibilitas-error"></strong>
                            </span>
                        </div>


                        <br><input type="button" onclick="hitung()" value="check" class="btn btn-warning"></input>
                        <!--<input type="reset" type="reset"></input>-->

                         <div class="form-group">
                            <label for="dsr">Dsr</label>
                            <input type="text" name="dsr" class="form-control">
                            <span class="text-danger">
                                <strong id="dsr-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Angsuran</label>
                            <input type="text" name="angsuran" class="form-control">
                            <span class="text-danger">
                                <strong id="angsuran-error"></strong>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="angsuran">Status</label>
                            <input type="text" name="status" class="form-control">
                            <span class="text-danger">
                                <strong id="status-error"></strong>
                            </span>
                        </div>


                        </form>
                   

                       
                        
                            <div class="form-group row">
                                <div class="col-sm-4 col-sm-offset-2">
                       <!--              <button class="btn btn-white btn-sm" type="submit">Cancel</button> -->
                                    <button class="btn btn-primary btn-simpan" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script>
    function hitung() {  
            var gaji= parseFloat(document.hitunggaji.gaji.value);  
            if (isNaN(gaji)) {gaji=0.0;}  
            var tunjangan_kerja=parseFloat(document.hitunggaji.tunjangan_kerja.value);  
            if (isNaN(tunjangan_kerja)) {tunjangan_kerja=0.0;}
            var insentif=parseFloat(document.hitunggaji.insentif.value);  
            if (isNaN(insentif)) {insentif=0.0;}
            var limit=parseFloat(document.hitunggaji.limit.value);  
            if (isNaN(limit)) {limit=0.0;}
            var tenor=parseFloat(document.hitunggaji.tenor.value);  
            if (isNaN(tenor)) {tenor=0.0;}
            var bunga=parseFloat(document.hitunggaji.bunga.value);  
            if (isNaN(bunga)) {bunga=0.0;}
            var gajitunjangan_kerja= ((((gaji * 100)/100) + ((tunjangan_kerja *80)/100) + ((insentif *80)/100)) *60 )/100 ;  
            var hasil = (((limit * 10) / 100) + limit) / tenor;
            var hasil_status =gajitunjangan_kerja > hasil ;
            document.hitunggaji.angsuran.value= hasil;
            document.hitunggaji.dsr.value= gajitunjangan_kerja; 
            document.hitunggaji.status.value= hasil_status;
            }

    var mem = $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    
    var form = $('#frmAddPengajuan');
    
    var done = function(resp) {
        // frmModal.modal('hide');
        // table.ajax.reload();
        $("#frmAddPengajuan")[0].reset();
    }

    $(function(){

    $('.btn-simpan').click(function(e) {
        e.preventDefault();

            var data = form.serialize();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: "{{ route('pengajuan.store')}}",
                method: 'POST',
                data: data,
                dataType: 'json',
                success: function(resp) {
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                     done(resp);
                },
                error: function(resp) {
                    if (resp.responseJSON.meta.message.agama){
                        $( '#agama-error' ).html( resp.responseJSON.meta.message.agama[0] );
                    }                    
                    if (resp.responseJSON.meta.message.alamat){
                        $( '#alamat-error' ).html( resp.responseJSON.meta.message.alamat[0] );
                    }
                   
                }
            });
            
            return false;
        });
    });

    
     
</script>
@endpush