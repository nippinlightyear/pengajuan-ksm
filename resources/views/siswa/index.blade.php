@extends('layouts.app')

@section('title', 'Kelola Data Siswa')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>List Siswa</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahSiswa()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered table-hover" id="siswaTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Jurusan</th>
                            <th>Alamat</th>
                            <th>Nama Orang Tua</th>
        
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalSiswa" tabindex="-1" role="dialog" aria-labelledby="modalSiswaLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmSiswa">
                    <div class="modal-body">
                        {{ csrf_field() }}
            
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div> 
                        <div class="form-group">
                            <label for="role_id">Pilih Kelas</label>
                            <select name="kelas" class="form-control" required>
                            @foreach($kelas as $kls)
                                <option value="{!! $kls->id !!}">{!! $kls->kelas !!}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role_id">Pilih Jurusan</label>
                            <select name="jurusan" class="form-control" required>
                            @foreach($jurusan as $jrs)
                                <option value="{!! $jrs->id !!}">{!! $jrs->jurusan !!}</option>
                            @endforeach
                            </select>
                        </div>    
                        <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            
                        </div>
                         <div class="form-group">
                            <label for="kode">Nama Orang Tua</label>
                            <input type="text" name="nama_ortu" class="form-control" max-length="3">
                           
                        </div>
                        <!-- <div class="form-group">
                            <label for="kode">Masukkan Foto</label>
                            <input type="file" name="avatar" id="avatar" class="form-control">
                        </div>     -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalSiswa');
    var form = $('#frmSiswa');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateSiswa('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteSiswa('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
        link: function(row, type, data){
            return '<a href="/siswa/'+data.id+'/profile">'+data.nama+'</a>';
        },
    };

    var resetForm = function() {
        method = 'POST';
        $('[name="nama"]').val('');
        $('[name="kelas"]').val('');
        $('[name="jurusan"]').val('');
        $('[name="alamat"]').val('');
        $('[name="nama_ortu"]').val('');
        $('[name="avatar"]').val('');
        $('.modal-title').text('Tambah Data Siswa');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahSiswa = function() {
        formAction = '{!! route('siswa.create') !!}';
        resetForm();
        frmModal.modal('show');
    }

    var updateSiswa = function(id) {
        $.ajax({
            url: 'siswa/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'siswa/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="nama"]').val(data.nama);
                $('[name="kelas"]').val(data.kelas);
                $('[name="jurusan"]').val(data.jurusan);
                $('[name="alamat"]').val(data.alamat);
                $('[name="nama_ortu"]').val(data.nama_ortu);
                // $('[name="avatar"]').val(data.avatar);
                // console.log()

                $('.modal-title').text('Edit Data Siswa');
                frmModal.modal('show');
            },
            error: function(resp) {
                frmModal.modal('show');
                error(resp);
            }
        })
    }

    var deleteSiswa = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'siswa/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }

    $(function(){   
        table = $('#siswaTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablesiswa') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                { data: 'id', render: styles.link },
                { data: 'kelas.kelas' },
                { data: 'jurusan.jurusan' },
                { data: 'alamat' },
                { data: 'nama_ortu' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
            
        });
        
        $('.btn-simpan').click(function(e) {
            e.preventDefault();
            // console.log(avatar)
            var data = form.serialize();
            
            $( '#nama-error' ).html( "" );
            $( '#kelas-error' ).html( "" );
            $( '#jurusan-error' ).html( "" );

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                dataType:'JSON',
                success: function(resp) {
                    done(resp);
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                
                error: function(resp) {
                    if (resp.responseJSON.meta.message.nama){
                        $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    }
                    if(resp.responseJSON.meta.message.kelas){
                        $( '#kelas-error' ).html( resp.responseJSON.meta.message.kelas[0] );
                    }
                    if(resp.responseJSON.meta.message.jurusan){
                    $( '#jurusan-error' ).html( resp.responseJSON.meta.message.jurusan[0] );   
                    }
                }
            });
            
            return false;
        });
    });
</script>
@endpush