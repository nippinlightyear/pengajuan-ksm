@extends('layouts.app')

@section('title', 'Kelola Data Kelas')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>List Kelas</h2>
        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12 white-bg">
                <div class="pull-right button-add">
                    <a href="#" role="modal" class="btn btn-sm btn-primary" onclick="tambahKelas()"><i class="fa fa-plus"></i></a>
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered" id="kelasTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Kelas</th>
                            <th>Aksi</th>
                            
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalKelas" tabindex="-1" role="dialog" aria-labelledby="modalKelasLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmKelas">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="nama">Kelas</label>
                            <input type="text" name="kelas" class="form-control" required>
                            <span class="text-danger">
                                <strong id="kelas-error"></strong>
                            </span>
                        </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalKelas');
    var form = $('#frmKelas');
    var formAction, table, uid;
    var method = 'POST';
    var styles = {
        button: function(row, type, data) {
            return '<a href="#" role="modal" onclick="updateKelas('+data.id+')" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i></a>&nbsp;<a href="#" class="btn btn-danger btn-sm" onclick="deleteKelas('+data.id+')"><i class="fa fa-trash"></i></a>';
        },
    };
    var resetForm = function() {
        method = 'POST';
        $('[name="kelas"]').val('');
        $('.modal-title').text('Tambah Data Kelas');
    }

    var done = function(resp) {
        frmModal.modal('hide');
        table.ajax.reload();
    }

    var tambahKelas = function() {
        formAction = '{!! route('kelas.create') !!}';
        resetForm();
        frmModal.modal('show');
    }
    var updateKelas = function(id) {
 
        $.ajax({
            url: 'kelas/'+id+'/edit',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            type: 'GET',
            data: { id: id },
            dataType: 'json',
            success: function(resp) {
                formAction = 'kelas/'+id+'/update';
                method = 'PUT';

                var data = resp.data;
                $('[name="kelas"]').val(data.kelas);
                $('.modal-title').text('Edit Data Kelas');
                frmModal.modal('show');
                },
            error: function(resp) {
                frmModal.modal('show');
                error(resp);
            }
        })
        }

        var deleteKelas = function(id) {
        swal({
            title: "Apa anda yakin?",
            text: "Data akan dihapus dan tidak bisa dikembalikan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        }, function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                url: 'kelas/'+id+'/delete',
                type: 'DELETE',
                data: { id: id },
                dataType: 'json',
                success: function(resp) {
                    done(resp);
                },
                error: function(resp) {
                    error(resp);
                }
            });
            swal("Berhasil!", "Data anda berhasil dihapus.", "success");
        });
    }
    $(function(){
            table = $('#kelasTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                url: '{!! route('tablekelas') !!}',
                type: 'POST',
            },
            columns: [
                { data: 'id' },
                { data: 'kelas' },
                { data: 'id', orderable: false, sClass:"text-center", render: styles.button }
            ]
        });
        $('.btn-simpan').click(function(e) {
            e.preventDefault();

            var data = form.serialize();
            
            
            $( '#kelas-error' ).html( "" );

            $.ajax({
                url: formAction,
                method: method,
                data: data,
                success: function(resp) {
                    done(resp);
                     swal({
                        title: "Berhasil!",
                        text: "Data berhasil disimpan!",
                        type: "success"
                    });
                },
                error: function(resp) {
                    // if (resp.responseJSON.meta.message.nama){
                    //     $( '#nama-error' ).html( resp.responseJSON.meta.message.nama[0] );
                    // }
                }
            });
            
            return false;
    });
    });

</script>
@endpush