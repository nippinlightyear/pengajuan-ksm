@extends('layouts.app')

@section('title', 'Kelola Data Siswa')

@push('styles')
<style>
.twitter-typeahead {
    display: block !important;
}
.list-group-item {
    background: #fff !important;
}
.tt-dataset {
    margin-top: -20px !important;
}
</style>
@endpush

@section('content')

<div class="main">
			<!-- MAIN CONTENT -->
			<div class="main-content">
				<div class="container-fluid">
					<div class="panel panel-profile">
						<div class="clearfix">
							<!-- LEFT COLUMN -->
							<div class="profile-left">
								<!-- PROFILE HEADER -->
								<div class="profile-header">
									<div class="overlay"></div>
									<div class="profile-main">
										<img src="{!! asset('images/saswi.png') !!}" class="img" alt="Avatar">
										<h3 class="name">{{$siswa->nama}}</h3>
                                        <span class="online-status status-available">Available</span>
									</div>
								</div>
							
								<!-- PROFILE DETAIL -->
								<div class="profile-detail">
									<div class="profile-info">
										<h4 class="heading">Basic Info</h4>
										<ul class="list-unstyled list-justify">
											<li>Kelas <span>{{$siswa->kelas}}</span></li>
											<li>Jurusan <span>{{$siswa->jurusan}}</span></li>
											<li>Alamat <span>{{$siswa->alamat}}</span></li>
											<li>Nama Orang Tua <span>{{$siswa->nama_ortu}}</span></li>
										</ul>
									</div>
									<div class="profile-info">
										<h4 class="heading">Social</h4>
										<ul class="list-inline social-icons">
											<li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#" class="google-plus-bg"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#" class="github-bg"><i class="fa fa-github"></i></a></li>
										</ul>
									</div>
									<div class="profile-info">
										<h4 class="heading">About</h4>
										<p>Interactively fashion excellent information after distinctive outsourcing.</p>
									</div>
								<!-- END PROFILE DETAIL -->
							</div>
							</div>
							<!-- END RIGHT COLUMN -->
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT -->
</div>
<div class="modal fade" id="modalSiswa" tabindex="-1" role="dialog" aria-labelledby="modalSiswaLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-title"></h4>
                </div>
                <form id="frmSiswa">
                    <div class="modal-body">
                        {{ csrf_field() }}
            
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" name="nama" class="form-control" required>
                            <span class="text-danger">
                                <strong id="nama-error"></strong>
                            </span>
                        </div> 
                        <div class="form-group">
                            <label for="role_id">Pilih Kelas</label>
                            <select name="kelas" class="form-control" required>
                            @foreach($kelas as $kls)
                                <option value="{!! $kls->id !!}">{!! $kls->kelas !!}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role_id">Pilih Jurusan</label>
                            <select name="jurusan" class="form-control" required>
                            @foreach($jurusan as $jrs)
                                <option value="{!! $jrs->id !!}">{!! $jrs->jurusan !!}</option>
                            @endforeach
                            </select>
                        </div>    
                        <div class="form-group">
                            <label for="kode">Alamat</label>
                            <input type="text" name="alamat" class="form-control" max-length="3">
                            
                        </div>
                         <div class="form-group">
                            <label for="kode">Nama Orang Tua</label>
                            <input type="text" name="nama_ortu" class="form-control" max-length="3">
                           
                        </div>
                                  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                        <input type="submit" class="btn btn-primary btn-simpan" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>


@stop
@push('scripts')
<script src="{!! asset('js/typeahead.bundle.min.js') !!}"></script>
<script>
    var frmModal = $('#modalSiswa');
    var form = $('#frmSiswa');
    var formAction, table, uid;
    var method = 'POST';
	var updateSiswa = function(id) {
 
 $.ajax({
	 url: id+'/edit',
	 headers: {
		 'X-CSRF-TOKEN': '{{ csrf_token() }}'
	 },
	 type: 'GET',
	 data: { id: id },
	 dataType: 'json',
	 success: function(resp) {
		 formAction = 'siswa/'+id+'/update';
		 method = 'PUT';

		 var data = resp.data;
		 $('[name="nama"]').val(data.nama);
		 $('[name="kelas"]').val(data.kelas);
		 $('[name="jurusan"]').val(data.jurusan);
		 $('[name="alamat"]').val(data.alamat);
		 $('[name="nama_ortu"]').val(data.nama_ortu);

		 $('.modal-title').text('Edit Data Siswa');
		 frmModal.modal('show');
	 },
	 error: function(resp) {
		//  frmModal.modal('show');
		//  error(resp);
	 }
 })
}
</script>
@endpush