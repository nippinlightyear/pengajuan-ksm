<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                            <img alt="image" class="img-circle" src="{!! asset('images/profile_small.jpg') !!}" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear">
                            <span class="block m-t-xs">
                                <strong class="font-bold">{{Auth::guard('admin')->user()->nama}}</strong>
                            </span>
                            <span class="text-muted text-xs block">User MKS<b class="caret"></b></span>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="{!! route('logout') !!}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    ED
                </div>
            </li>
            <li class="{{ isActiveRoute('main') }}">
                <a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> <span class="nav-label">Beranda</span></a>
            </li>
            
             <li class="{{ isActiveRoute('users') }}">
                <a href="{{ url('/users') }}"><i class="fa fa-book"></i> <span class="nav-label">Users</span> </a>
            </li>
            @if (Auth::user()->role_id == 2)
             <!-- <li class="{{ isActiveRoute('training') }}">
                <a href="{{ url('/training') }}"><i class="fa fa-book"></i> <span class="nav-label">Training</span> </a>
            </li>
             <li class="{{ isActiveRoute('pengajuan') }}">
                <a href="{{ url('/pengajuan') }}"><i class="fa fa-book"></i> <span class="nav-label">Pengajuan</span> </a>
            </li> -->
            <li class="{{ isActiveRoute('siswa') }}">
                <a href="{{ url('/siswa') }}"><i class="fa fa-book"></i> <span class="nav-label">Data Siswa</span></a>
            </li>
            <li class="{{ isActiveRoute('jurusan') }}">
                <a href="{{ url('/jurusan') }}"><i class="fa fa-book"></i> <span class="nav-label">Data Jurusan</span></a>
            </li>
            <li class="{{ isActiveRoute('kelas') }}">
                <a href="{{ url('/kelas') }}"><i class="fa fa-book"></i> <span class="nav-label">Data Kelas</span></a>
            </li>
            <li class="{{ isActiveRoute('pelajaran') }}">
                <a href="{{ url('/pelajaran') }}"><i class="fa fa-book"></i> <span class="nav-label">Data Pelajaran</span></a>
            </li>
            <li class="{{ isActiveRoute('guru') }}">
                <a href="{{ url('/guru') }}"><i class="fa fa-book"></i> <span class="nav-label">Data Guru</span></a>
            </li>
            <li class="{{ isActiveRoute('jadwal') }}">
                <a href="{{ url('/jadwal') }}"><i class="fa fa-book"></i> <span class="nav-label">Jadwal Pelajaran</span></a>
            </li>
            @endif

            @if (Auth::user()->role_id == 1)
             <li class="{{ isActiveRoute('training') }}">
                <a href="{{ url('/training') }}"><i class="fa fa-book"></i> <span class="nav-label">Training</span> </a>
             </li>
             <li class="{{ isActiveRoute('karyawan') }}">
                <a href="{{ url('/karyawan') }}"><i class="fa fa-book"></i> <span class="nav-label">Karyawan</span> </a>
            </li>
            @endif

            <!--
            <li class="{{ isActiveRoute('formulir') }}">
                <a href="{{ url('/formulir') }}"><i class="fa fa-book"></i> <span class="nav-label">Formulir</span> </a>
            </li>


            <li class="{{ isActiveRoute('jabatan') }}">
                <a href="{{ url('/jabatan') }}"><i class="fa fa-book"></i> <span class="nav-label">Jabatan</span> </a>
            </li>
<-->
        
            </li>
        </ul>

    </div>
</nav>
