<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MembuatTablePengajuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama');
            $table->string('nik');
            $table->string('alamat');
            $table->string('nama_perusahaan');
            $table->string('gaji');
            $table->string('tunjangan_kerja');
            $table->string('insentif');
            $table->string('limit');
            $table->string('tenor');
            $table->string('bunga');
            $table->string('kolektibilitas');
            $table->string('dsr');
            $table->string('angsuran');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan');
    }
}
