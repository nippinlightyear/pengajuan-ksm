<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MembuatTableTraining extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_rek');
            $table->string('nama_nasabah');
            $table->string('alamat');
            $table->string('no_tlp');
            $table->string('nama_perusahaan');
            $table->string('jenis_perusahaan');
            $table->string('limit');
            $table->string('kolektibilitas');
            $table->string('tenor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training');
    }
}
